import Bee from 'bee-queue';
import RegistryMail from '../app/jobs/RegistryMail';
import HelpAnswerMail from '../app/jobs/HelpAnswerMail';
import redisConfig from '../config/redis';

const jobs = [RegistryMail, HelpAnswerMail];

class Queue {
  constructor() {
    this.queues = {};

    this.init();
  }

  // foreach job a queue is created and a bee key is created inside the queue
  init() {
    jobs.forEach(({ key, handle }) => {
      this.queues[key] = {
        bee: new Bee(key, {
          redis: redisConfig,
        }),
        handle,
      };
    });
  }

  // adds the job to the queue
  add(queue, job) {
    return this.queues[queue].bee.createJob(job).save();
  }

  // process the queue
  processQueue() {
    jobs.forEach(job => {
      const { bee, handle } = this.queues[job.key];

      bee.process(handle);
    });
  }
}

export default new Queue();
