import { Router } from 'express';

import PlanController from '../app/controllers/PlanController';
import AuthMiddleware from '../app/middlewares/auth';

const routes = Router();

routes.use(AuthMiddleware);
routes.get('/', PlanController.index);
routes.post('/', PlanController.store);
routes.put('/:id', PlanController.update);
routes.delete('/:id', PlanController.delete);

export default routes;
