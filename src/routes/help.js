import { Router } from 'express';
import HelpController from '../app/controllers/HelpController';
import AuthMiddleware from '../app/middlewares/auth';

const routes = Router();

routes.use(AuthMiddleware);

// help-request
routes.get('/', HelpController.index);
routes.put('/:id', HelpController.update);

export default routes;
