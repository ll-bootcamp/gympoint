import { Router } from 'express';

import RegisterController from '../app/controllers/RegisterController';
import AuthMiddleware from '../app/middlewares/auth';

const routes = Router();

routes.use(AuthMiddleware);
routes.get('/', RegisterController.index);
routes.post('/', RegisterController.store);
routes.put('/:id', RegisterController.update);
routes.delete('/:id', RegisterController.delete);

export default routes;
