import { Router } from 'express';

import sessions from './sessions';
import students from './students';
import plans from './plans';
import registries from './registries';
import help from './help';

const routes = Router();
routes.use('/sessions', sessions);
routes.use('/students', students);
routes.use('/plans', plans);
routes.use('/registries', registries);
routes.use('/help-requests', help);

export default routes;
