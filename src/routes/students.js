import { Router } from 'express';

import StudentController from '../app/controllers/StudentController';
import CheckinController from '../app/controllers/CheckinController';
import HelpController from '../app/controllers/HelpController';
import AuthMiddleware from '../app/middlewares/auth';

const routes = Router();

// Help Requests
routes.get('/:id/help-requests', HelpController.show);
routes.post('/:id/help-requests', HelpController.store);

// Check-in
routes.post('/:id/checkins', CheckinController.store);
routes.get('/:id/checkins', CheckinController.index);

// Authenticated routes
routes.use(AuthMiddleware);

// Crud Student
routes.get('/', StudentController.index);
routes.post('/', StudentController.store);
routes.put('/:id', StudentController.update);

export default routes;
