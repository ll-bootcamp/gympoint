import { Router } from 'express';
const routes = Router();

import SessionController from '../app/controllers/SessionController';

routes.post('/', SessionController.store);

export default routes;
