import * as Yup from 'yup';

import Plan from '../models/Plan';

const validateRequest = async body => {
  const schema = Yup.object().shape({
    title: Yup.string().min(3).required(),
    duration: Yup.number().required(),
    price: Yup.number().required(),
  });

  const response = await schema.isValid(body);
  return response;
};

class PlanController {
  async index(req, res) {
    const plans = await Plan.findAll();

    return res.json(plans);
  }

  async store(req, res) {
    const validation = await validateRequest(req.body);

    if (!validation) {
      return res.status(400).json({ error: "Validation failed" })
    }

    const { title, duration, price } = req.body;

    const plan = await Plan.create({
      title,
      duration,
      price
    });

    return res.json(plan);
  }

  async update(req, res) {

    const plan = await Plan.findByPk(req.params.id)
    if (!plan) {
      return res.status(404).json({ error: "Plan not found!" })
    }

    const validation = await validateRequest(req.body)
    if (!validation) {
      return res.status(400).json({ error: "Validation failed" })
    }

    const { title, duration, price } = req.body;

    const newPlan = await plan.update({ title, duration, price })

    return res.json(newPlan);
  }

  async delete(req, res) {
    const plan = await Plan.findByPk(req.params.id)

    if (!plan) {
      return res.status(404).json({ error: "Plan not found!" })
    }

    await plan.destroy();

    return res.status(200).send();
  }
}

export default new PlanController();
