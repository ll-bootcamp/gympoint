import * as Yup from 'yup';

import User from '../models/User';

const validateRequest = async (body, res) => {
  const schema = Yup.object().shape({
    name: Yup.string()
      .required()
      .min(3),
    email: Yup.string()
      .email()
      .required(),
    password: Yup.string()
      .required()
      .min(4),
  });

  return schema.isValid(body);
};

const checkEmailExist = async (email, id = null) => {
  return await User.findOne({ where: { email } });
};

class UserController {
  async store(req, res) {
    const isValid = await validateRequest(req.body, res);
    if (!isValid) {
      return res.status(400).json({ error: 'Data validation failed!' });
    }

    const request = req.body;

    if (await checkEmailExist(request.email)) {
      return res.status(400).json({ error: 'E-mail already exists' });
    }

    return res.status(200).send();
  }
}

export default new UserController();
