import * as Yup from 'yup';

import Student from '../models/Student';

const validateRequest = async body => {
  const schema = Yup.object().shape({
    name: Yup.string()
      .min(3)
      .required(),
    email: Yup.string()
      .email()
      .required(),
    weight: Yup.number(),
    height: Yup.number(),
  });

  const response = await schema.isValid(body);
  return response;
};

const checkEmailExists = async (email, id = null) => {
  const student = await Student.findOne({ where: { email } });
  if (student) {
    if (!id || (student.id != id && id !== null)) {
      return true;
    }
  }
  return false;
};

class StudentController {
  async index(req, res) {
    const students = await Student.findAll();

    if (students) {
      return res.json(students || []);
    }

    return res.status(404).json({ error: 'No students registered' });
  }

  async store(req, res) {
    const validation = await validateRequest(req.body);

    if (!validation) {
      return res.status(400).json({ error: 'Validation failed' });
    }

    // validating if the email informed is already in use
    if (await checkEmailExists(req.body.email)) {
      return res.status(400).json({ error: 'E-mail already used' });
    }

    // saving student data
    const student = await Student.create(req.body);

    return res.json(student);
  }

  async update(req, res) {
    const validation = await validateRequest(req.body);

    if (!validation) {
      return res.status(400).json({ error: 'Validation failed' });
    }

    const { id } = req.params;
    const student = await Student.findByPk(id);

    if (!student) {
      return res.status(404).json({ error: 'Student not found' });
    }

    if (await checkEmailExists(req.body.email, id)) {
      return res.status(400).json({ error: 'E-mail already used' });
    }

    const newData = await student.update(req.body);

    return res.json(newData);
  }
}

export default new StudentController();
