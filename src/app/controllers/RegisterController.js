import * as Yup from 'yup';
import { isBefore, parseISO, addDays, addMonths, endOfDay } from 'date-fns';
import { Op } from 'sequelize';

import Queue from '../../lib/queue';
import RegistryMail from '../jobs/RegistryMail';

import Register from '../models/Register';
import Student from '../models/Student';
import Plan from '../models/Plan';

const validateRequest = async body => {
  const schema = Yup.object().shape({
    student_id: Yup.number().required(),
    plan_id: Yup.number().required(),
    start_date: Yup.date().required(),
  });

  const response = await schema.isValid(body);
  return response;
};

class RegisterController {
  async index(req, res) {
    const { page = 1 } = req.query;

    const registries = await Register.findAll({
      order: ['start_date'],
      limit: 20,
      offset: (page - 1) * 20,
      attributes: ['id', 'plan_id', 'student_id', 'start_date', 'end_date', 'overdue'],
      include: [
        {
          model: Student,
          as: 'student',
          attributes: ['id', 'name'],
        },
        {
          model: Plan,
          as: 'plan',
          attributes: ['id', 'title', 'price', 'duration'],
        },
      ],
    });

    return res.json(registries);
  }

  async store(req, res) {
    const validation = await validateRequest(req.body);

    if (!validation) {
      return res.status(422).json({ error: 'Validation failed' });
    }

    // Verifies if student id informed exists
    const student = await Student.findByPk(req.body.student_id);
    if (!student) {
      return res.status(404).json({ error: 'Student not found' });
    }

    // Verifies if plan id informed exists
    const plan = await Plan.findByPk(req.body.plan_id);
    if (!plan) {
      return res.status(404).json({ error: 'Plan not found' });
    }

    const startDate = parseISO(req.body.start_date);
    const endDate = addMonths(startDate, plan.duration);
    const price = plan.price * plan.duration;

    // Verifies if Student is already registered to a plan and it is not overdue
    const checkStudentRegistries = await Register.findOne({
      where: {
        student_id: student.id,
        end_date: {
          [Op.gt]: endOfDay(startDate),
        },
      },
    });

    if (checkStudentRegistries) {
      return res.status(403).json({ error: 'Student is already registered to a plan' });
    }

    // Verifies if the start date is higher than today
    if (!isBefore(new Date(), addDays(startDate, 1))) {
      return res.status(422).json({ error: 'Register date should be at least today' });
    }

    const registration = await Register.create({
      student_id: student.id,
      plan_id: plan.id,
      start_date: startDate,
      end_date: endDate,
      price,
    });

    await Queue.add(RegistryMail.key, {
      student,
      plan,
      registration,
    });

    return res.json(registration);
  }

  async update(req, res) {
    const validation = await validateRequest(req.body);
    if (!validation) {
      return res.status(422).json({ error: 'Validation failed' });
    }

    const registry = await Register.findByPk(req.params.id);
    if (!registry) {
      return res.status(404).json({ error: 'Registry not found' });
    }

    // verify if student has other registries before due to
    const checkRegistries = await Register.findOne({
      where: {
        student_id: req.body.student_id,
        id: { [Op.gt]: registry.id },
      },
    });

    if (checkRegistries) {
      return res
        .status(403)
        .json({ error: `The student has a newer registry: ${checkRegistries.id}` });
    }

    // Verifies if student id informed exists
    const student = await Student.findByPk(req.body.student_id);
    if (!student) {
      return res.status(404).json({ error: 'Student not found' });
    }

    // Verifies if plan id informed exists
    const plan = await Plan.findByPk(req.body.plan_id);
    if (!plan) {
      return res.status(404).json({ error: 'Plan not found' });
    }

    if (registry.student_id !== student.id) {
      return res.status(401).json({ error: 'Unable to switch students' });
    }

    let startDate = parseISO(req.body.start_date);

    if (isBefore(startDate, new Date()) && registry.start_date != startDate) {
      startDate = addDays(new Date(), 1);
    }

    const endDate = addMonths(startDate, plan.duration);
    const price = plan.price * plan.duration;

    if (plan.id !== registry.plan_id) {
      registry.end_date = new Date();
      registry.save();

      const newRegistry = await Register.create({
        student_id: student.id,
        plan_id: plan.id,
        start_date: startDate,
        end_date: endDate,
        price,
      });

      return res.json(newRegistry);
    }

    await registry.update({
      student_id: student.id,
      plan_id: plan.id,
      start_date: startDate,
      end_date: endDate,
      price,
    });

    return res.json(registry);
  }

  async delete(req, res) {
    const registry = await Register.findByPk(req.params.id);

    if (!registry) {
      return res.status(404).json({ error: 'Registry not found!' });
    }

    await registry.destroy();
    return res.status(200).send();
  }
}

export default new RegisterController();
