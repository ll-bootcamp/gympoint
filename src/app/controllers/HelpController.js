import * as Yup from 'yup';
import Queue from '../../lib/queue';

import Student from '../models/Student';
import Help from '../models/Help';
import HelpAnswerMail from '../jobs/HelpAnswerMail';

class HelpController {
  async index(req, res) {
    const requests = await Help.findAll({
      where: {
        answered_at: null,
      },
      attributes: ['id', 'question', 'created_at', 'updated_at'],
      include: [
        {
          model: Student,
          as: 'student',
          attributes: ['id', 'name', 'email'],
        },
      ],
    });

    return res.json(requests);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      question: Yup.string()
        .min(3)
        .required(),
    });

    const validation = await schema.isValid(req.body);
    if (!validation) {
      return res.json({ error: 'Validation fails!' });
    }

    const { question } = req.body;
    const { id } = req.params;

    if (!(await Student.findByPk(id))) {
      return res.status(404).json({ error: 'Student not found' });
    }

    const help = await Help.create({
      question,
      student_id: id,
    });

    return res.json(help);
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      answer: Yup.string()
        .min(3)
        .required(),
    });

    const validation = await schema.isValid(req.body);
    if (!validation) {
      return res.json({ error: 'Validation fails!' });
    }

    const help = await Help.findByPk(req.params.id);
    if (!help) {
      return res.status(404).json({ error: 'Request not found!' });
    }

    if (help.answer !== null) {
      return res.status(401).json({ error: 'This question has already been answered!' });
    }

    await help.update({ answer: req.body.answer, answered_at: new Date() });

    const student = await Student.findByPk(help.student_id);

    await Queue.add(HelpAnswerMail.key, {
      student,
      help,
    });

    return res.json(help);
  }

  async show(req, res) {
    const { id } = req.params;

    if (!(await Student.findByPk(id))) {
      return res.status(404).json({ error: 'Student not found!' });
    }

    const requests = await Help.findAll({
      where: {
        student_id: id,
      },
      attributes: ['id', 'question', 'answer', 'created_at', 'answered_at'],
      include: [
        {
          model: Student,
          as: 'student',
          attributes: ['id', 'name', 'email'],
        },
      ],
    });

    return res.json(requests);
  }
}

export default new HelpController();
