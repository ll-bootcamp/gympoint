import Mail from '../../lib/mail';
import { resolve } from 'path';
import { format, parseISO } from 'date-fns';

class RegistryMail {
  get key() {
    return 'RegistryMail';
  }

  async handle({ data }) {
    const { student, plan, registration } = data;

    const start = format(parseISO(registration.start_date), 'dd/MM/yyyy');
    const end = format(parseISO(registration.end_date), 'dd/MM/yyyy');

    await Mail.sendMail({
      to: `${student.name} <${student.email}>`,
      subject: 'Matricula GymPoint',
      template: 'registry',
      context: {
        student: student.name,
        plan: plan.title,
        start,
        end,
        duration: plan.duration,
        price: plan.price.toFixed(2),
        total: registration.price.toFixed(2),
      },
      attachments: [
        {
          filename: 'logo.png',
          path: resolve(__dirname, '..', 'views', 'emails', 'assets', 'img', 'logo.png'),
          cid: 'logo',
        },
      ],
    });
  }
}

export default new RegistryMail();
