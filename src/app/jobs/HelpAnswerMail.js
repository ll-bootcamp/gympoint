import Mail from '../../lib/mail';
import { resolve } from 'path';
import { format, parseISO } from 'date-fns';

class HelpAnswerMail {
  get key() {
    return 'HelpAnswerMail';
  }

  async handle({ data }) {
    const { student, help } = data;

    const questioned_at = format(parseISO(help.createdAt), "dd/MM/yyyy ' - ' H:m");
    const answered_at = format(parseISO(help.answered_at), "dd/MM/yyyy ' - ' H:m");

    await Mail.sendMail({
      to: `${student.name} <${student.email}>`,
      subject: 'GymPoint - Help Request',
      template: 'help_answer',
      context: {
        student: student.name,
        question: help.question,
        questioned_at,
        answer: help.answer,
        answered_at,
      },
      attachments: [
        {
          filename: 'logo.png',
          path: resolve(__dirname, '..', 'views', 'emails', 'assets', 'img', 'logo.png'),
          cid: 'logo',
        },
      ],
    });
  }
}

export default new HelpAnswerMail();
